﻿using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Concrete
{
    public class ProfileRepository : IProfileRepository
    {
        public async Task<bool> Insert(Profile profile) 
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                var existedProfile = await bnbjoyBackendDbEntities.Profiles.AsNoTracking().Where(p => p.UserId == profile.UserId).FirstOrDefaultAsync();
                if (existedProfile == null)
                {
                    bnbjoyBackendDbEntities.Profiles.Add(profile);
                    return await bnbjoyBackendDbEntities.SaveChangesAsync() > 0;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
