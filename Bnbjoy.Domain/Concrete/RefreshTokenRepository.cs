﻿using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Concrete
{
    public class RefreshTokenRepository : IRefreshTokenRepository
    {

        public async Task<RefreshToken> FindById(string rToken)
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                return await bnbjoyBackendDbEntities.RefreshTokens.AsNoTracking().Where(x => x.RToken == rToken).FirstOrDefaultAsync();
            }
        }

        public async Task<bool> Insert(RefreshToken refreshToken)
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                //通过ClientId和UserName来查找refresh token，如果存在的话，先移除
                using (var transaction = bnbjoyBackendDbEntities.Database.BeginTransaction())
                {
                    try
                    {
                        var existingToken = await bnbjoyBackendDbEntities.RefreshTokens.AsNoTracking().Where(r => r.UserName == refreshToken.UserName && r.ClientId == refreshToken.ClientId).FirstOrDefaultAsync();
                        if (existingToken != null)
                        {
                            var dResult = await Delete(existingToken);
                        }
                        bnbjoyBackendDbEntities.RefreshTokens.Add(refreshToken);
                        bool iResult = await bnbjoyBackendDbEntities.SaveChangesAsync() > 0;
                        transaction.Commit();
                        return iResult;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public async Task<bool> Delete(RefreshToken rTokenObj)
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                bnbjoyBackendDbEntities.RefreshTokens.Attach(rTokenObj);
                bnbjoyBackendDbEntities.RefreshTokens.Remove(rTokenObj);
                return await bnbjoyBackendDbEntities.SaveChangesAsync() > 0;
            }
        }

        public async Task<bool> Delete(string rToken)
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                var target = await bnbjoyBackendDbEntities.RefreshTokens.AsNoTracking().Where(x => x.RToken == rToken).FirstOrDefaultAsync();
                if (target != null)
                {
                    bnbjoyBackendDbEntities.RefreshTokens.Attach(target);
                    bnbjoyBackendDbEntities.RefreshTokens.Remove(target);
                    await bnbjoyBackendDbEntities.SaveChangesAsync();
                }
                return true;
            }
        }

        public async Task<List<RefreshToken>> GetAllRefreshTokens()
        {
            using (BnbjoyBackendDbEntities bnbjoyBackendDbEntities = new BnbjoyBackendDbEntities())
            {
                return await bnbjoyBackendDbEntities.RefreshTokens.AsNoTracking().ToListAsync();
            }
        }
    }
}
