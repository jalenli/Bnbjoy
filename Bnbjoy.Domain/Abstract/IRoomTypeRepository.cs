﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IRoomTypeRepository
    {
        Task<bool> UpRank(string bnbId, int currentRank);

        Task<bool> DownRank(string bnbId, int currentRank);

        Task<dynamic> FindRoomType(string bnbId, string roomTypeName);

        Task<bool> InsertRoomType(RoomType roomType, IEnumerable<Room> rooms, CommonPrice commonPrice);

        Task<IEnumerable<dynamic>> ListAllRoomTypes(string bnbId);

        Task<bool> UpdateDailyPrice(DateTime fromDate, DateTime toDate, string roomTypeId, decimal price);

        Task<List<DailyPrice>> ListDailyPrice(DateTime dateTime, string roomTypeId);
        
        Task<List<DailyPrice>> ListDailyPrice(DateTime fromTime, DateTime toTime, string roomTypeId);

        Task<CommonPrice> RetrieveCommonPrice(string roomTypeId);

        Task<List<CommonPrice>> RetrieveCommonPriceByBnbId(string bnbId);

        Task<List<DailyPrice>> ListDailyPriceByBnbId(DateTime fromTime, DateTime toTime, string BnbId);

        Task<List<dynamic>> ListBasicRoomsInfo(DateTime fromTime, DateTime toTime, string bnbId);
    }
}
