﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IOrderRepository
    {
        Task<bool> InsertOrder(Order order, IEnumerable<Customer> customers, IEnumerable<OrderSpending> spendings);

        Task<dynamic> FindOrder(string orderId);

        Task<string> ObtainMobileArea(string mobileNumber);

        Task<int> TotalRoomCount(string bnbId);
    
        Task<IEnumerable<dynamic>> RetrieveAvailableRooms(string bnbId, DateTime fromDate);
    }
}
