﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Domain.Abstract
{
    public interface IRefreshTokenRepository
    {
        Task<RefreshToken> FindById(string Id);

        Task<bool> Insert(RefreshToken refreshToken);

        Task<bool> Delete(RefreshToken rTokenObj);

        Task<bool> Delete(string Id);

        Task<List<RefreshToken>> GetAllRefreshTokens();
    }
}
