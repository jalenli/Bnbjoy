﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BnbjoyBackend.Site.ConstantsNS
{
    public class Constants
    {
        public const string PublicAccessTokenSession = "Public_Access_Token";
        public const string AccessTokenSession = "Access_Token";
        public const string RefreshTokenSession = "Refresh_Token";
        public const string OnDomainBnbsSession = "On_Domain_Bnbs_Session";
        public const string OnSelectBnbSession = "On_Select_Bnb";
        public const string AuthedUserInfo = "Authed_User_Info";
        public const string AccessTokenCookie = "buat";
        public const string RefreshTokenCookie = "burt";
        
        //Dashboard
        public const string DashboardHeadInfo = "Dashboard_Head_Info";

        //权限管理，一级菜单
        public const string DashboardManagement = "dashmgmt";
        public const string OrdersInfoManagement = "ordermgmt";
        public const string BnbInfoManagement = "infomgmt";
        public const string RoomInfoManagement = "roommgmt";
        public const string SocialManagement = "socialmgmt";
        public const string CustomerInfoManagement = "customermgmt";
        public const string StatisticsInfoManagement = "statsmgmt";
        public const string SettingsManagement = "settingsmgmt";

        public static string MainNavUrls = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "home/index", "ordersinfo/index", "bnbinfo/index", "roomtype/index", "socialinfo/index", "customersinfo/index", "statsinfo/index", "settings/index");

        public static string AuthKeys = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", DashboardManagement, OrdersInfoManagement, BnbInfoManagement, RoomInfoManagement, SocialManagement, CustomerInfoManagement, StatisticsInfoManagement, SettingsManagement);
        public static string AuthValues = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "前台管理", "订单信息", "民宿信息", "房价房态", "社交管理", "客人信息", "信息统计", "设置");
        public static string AuthUrls = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "home/*", "ordersinfo/*", "bnbinfo/*", "roomtype/*", "socialinfo/*", "customersinfo/*", "statsinfo/*", "settings/*");
        

        //二级菜单

        //设置
        public const string AccountManagement = "accountmgmt";
        public const string PaymentManagement = "paymentmgmt";
        public const string PersonalManagement = "personalmgmt";

        //房价房态
        public const string RoomTypeManagement = "roomtypemgmt";
        public const string RoomPriceManagement = "roompricemgmt";
    }
}