/**
 * Created by lijizhuang on 16/11/1.
 */
define(['jquery','underscore', 'backbone'], function ($, _, Backbone) {
    var BnbPermissionItem = Backbone.Model.extend({
        defaults:{
            bnbId:'',
            bnbName:'',
            employeeid:'',
            enabled:'',
        },

        initialize: function () {

        }
    });
    return BnbPermissionItem;
});