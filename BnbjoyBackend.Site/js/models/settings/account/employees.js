/**
 * Created by lijizhuang on 16/9/14.
 */
define(['jquery', 'underscore', 'backbone', 'models/settings/account/employee'], function ($, _, Backbone, Employee) {
    var Employees = Backbone.Collection.extend({
        model: Employee,

        initialize: function () {
            
        }
    });

    return Employees;
});