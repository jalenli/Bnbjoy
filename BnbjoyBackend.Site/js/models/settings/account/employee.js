/**
 * Created by lijizhuang on 16/9/14.
 */
define(['jquery','underscore', 'backbone'], function ($, _, Backbone) {
    var Employee = Backbone.Model.extend({
        defaults:{
            employeeid:'',
            userName:'',
            password:'',
            realName:'',
            email:'',
            mobile:'',
            permission:'',
            enabled:false
            //权限格式如下
            //permission:[
            //    {
            //        bnbName:'',
            //        rights:[{dashmgmt:true}, {ordermgmt:true}, {infomgmt:true}, {roommgmt:true}, {socialmgmt:true}, {customermgmt:true}, {statsmgmt:true}, {settingsmgmt:true}]
            //    }
            //]
        },

        
        initialize: function () {
            
        }
    });

    return Employee;
});