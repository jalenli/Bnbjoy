/**
 * Created by lijizhuang on 16/9/22.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
        var dailyPrice = Backbone.Model.extend({
            defaults:{
                date:'',
                dayOfWeek:'',
                festival:'',
                price:0,
                canChangePrice:true
            },
            
            initialize: function () {

            }
        });

        return dailyPrice;
});