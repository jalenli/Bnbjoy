/**
 * Created by lijizhuang on 16/9/19.
 */
define(['jquery', 'underscore', 'backbone', 'models/roomTypesMgr/roomType'], function ($, _, Backbone, RoomType) {
    var roomTypes = Backbone.Collection.extend({
        model:RoomType,
        
        initialize: function () {

        }
    });

    return roomTypes;
});