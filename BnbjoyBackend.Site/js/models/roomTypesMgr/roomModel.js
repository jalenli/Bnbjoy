/**
 * Created by lijizhuang on 16/9/19.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
        var roomModel = Backbone.Model.extend({
            defaults:{
                roomId:'',        //房间id
                roomNumber:'',    //房间号
                roomTypeId:'',    //房型id
                roomTypeName:'', //房型名称
                bnbId:'', //民宿id
                rank: '', //房间排名
                enabled:'' //是否启用
            },
            
            initialize: function () {
                
            }
        });

        return roomModel;
});