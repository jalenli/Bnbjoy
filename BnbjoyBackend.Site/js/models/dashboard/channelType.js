/**
 * Created by lijizhuang on 16/9/9.
 */
define([], function () {
    var ChannelType = {
        Online: 'Online',  //线上
        Offline: 'Offline' //线下
    };

    return ChannelType;
});