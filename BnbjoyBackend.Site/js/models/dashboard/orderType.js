/**
 * Created by lijizhuang on 16/11/18.
 */
define([], function () {
    var OrderType = {
        BO: 'BO', //住宿订单
        AO: 'AO',   //活动订单
        PO: 'PO'   //套餐订单
    };

    return OrderType;
});