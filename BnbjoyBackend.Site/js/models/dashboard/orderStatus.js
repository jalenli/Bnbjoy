/**
 * Created by lijizhuang on 16/11/18.
 */
define([], function () {
    var OrderStatus = {
        UnConfirmation: 'UnConfirmation', //未确认
        Confirmation: 'Confirmation',   //已确认
        Processing: 'Processing',   //处理中
        Completed: 'Completed',     //完成
        Canceled: 'Canceled'        //取消
    };

    return OrderStatus;
});