/**
 * Created by lijizhuang on 16/9/9.
 */
define([], function () {
    //var PaymentType = {
    //    Cash: '现金',
    //    AliPay: '支付宝',
    //    WechatPay: '微信支付',
    //    CyberBank: '网银',
    //    CreditCard: '信用卡',
    //    DebitCard: '借记卡'
    //};

    var PaymentType = {
        PIA: 'PIA', //预付
        PIC: 'PIC', //现付
    };

    return PaymentType;
});