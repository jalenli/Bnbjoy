/**
 * Created by lijizhuang on 16/11/18.
 */
define([], function () {
    var SpendItemType = {
        RoomFee: 'RoomFee', //房费
        Deposit: 'Deposit',   //押金
        Activity: 'Activity',   //活动
        Others: 'Others',     //其它
    };

    return SpendItemType;
});