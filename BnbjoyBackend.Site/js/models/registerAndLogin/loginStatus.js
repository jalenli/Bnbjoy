/**
 * Created by lijizhuang on 16/8/30.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var LoginStatus = Backbone.Model.extend({
        defaults: {
            loggedIn: false,
            token: null
        },

        initialize: function () {
            this.bind('change:token', this.onTokenChange, this);
            this.set({'token': localStorage.getItem('token')});
        },

        onTokenChange: function (status, token) {
            this.set({'loggedIn': !!token});
        },

        setToken: function(token) {
            localStorage.setItem('token', token);
            this.set({'token': token});
        }
    });

    return LoginStatus;
});