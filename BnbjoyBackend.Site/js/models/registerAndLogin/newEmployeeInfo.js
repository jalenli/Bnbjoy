/**
 * Created by lijizhuang on 16/10/31.
 */
define(['jquery', 'underscore', 'backbone', 'utils/regutil'], function ($, _, Backbone, RegUtil) {
    var NewEmployeeInfo = Backbone.Model.extend({
        defaults: {
            employeeid:'',
            userName:'',
            password:'',
            email:'',
            mobile: '',
            roleName: '',
            realName: '',
            ownerId: '',
            enabled: false,
        },

        validate: function(attributes, option){
            var errorArray = [];

            //帐号验证
            if ($.trim(attributes.userName).length === 0) {
                errorArray.push({item:"accountErr", error: "用户帐号不能为空"});
            }
            else if (!RegUtil.accountReg.test(attributes.userName)) {
                errorArray.push({item:"accountErr", error: "帐号应为4-16位的数字和字母组成"});
            }

            //密码验证
            if($.trim(attributes.password).length === 0){
                errorArray.push({item:"passwordErr", error: "用户密码不能为空"});
            }
            else if(!RegUtil.passwordReg.test(attributes.password)){
                errorArray.push({item:"passwordErr", error: "密码应为8-30位的字母、数字和特殊字符组成"});
            }

            //用户真实姓名验证
            if ($.trim(attributes.realName).length === 0) {
                errorArray.push({ item: "emailErr", error: "姓名不能为空" });
            }
            else if (!RegUtil.chineseNameReg.test(attributes.realName)) {
                errorArray.push({ item: "realNameError", error: "姓名格式错误" });
            }

            //用户手机号验证
            if ($.trim(attributes.mobile).length === 0) {
                errorArray.push({ item: "mobileErr", error: "掌柜手机号不能为空" });
            }
            else if (!RegUtil.mobileReg.test(attributes.mobile)) {
                errorArray.push({ item: 'mobileErr', error: '手机号格式不正确' });
            }

            //电子邮箱验证
            if($.trim(attributes.email).length === 0){
                errorArray.push({item:"emailErr", error: "用户邮箱不能为空"});
            }
            else if(!RegUtil.emailReg.test(attributes.email)){
                errorArray.push({item:"emailErr", error: "电子邮箱的格式不正确"});
            }

            //有一个或多个错误, 返回之
            if(errorArray.length > 0){
                return JSON.stringify(errorArray);
            }
        }
    });

    return NewEmployeeInfo;
});