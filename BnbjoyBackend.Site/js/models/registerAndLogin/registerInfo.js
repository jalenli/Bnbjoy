/**
 * Created by lijizhuang on 16/9/2.
 */
define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var RegisterInfo = Backbone.Model.extend({
        defaults : {
            userName:'',
            password:'',
            email:'',
            mobileNumber: '',
            mobileCaptcha: '',
            roleName: '',
            ownerId: '',
        },

        //发送注册信息至服务端
        send: function () {

        },

        validate: function(attributes, option){
            var errorArray = [];

            var accountReg =/^([\u4e00-\u9fa5]{2,8}|[0-9a-zA-Z_]{4,16})$/;
            var passwordReg = new RegExp('(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}');
            var emailReg = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/;
            var mobileReg = /^1[3|4|5|7|8]\d{9}$/;

            //帐号验证
            if ($.trim(attributes.userName).length === 0) {
                errorArray.push({item:"accountErr", error: "用户帐号不能为空"});
            }
            else if (!accountReg.test(attributes.userName)) {
                errorArray.push({item:"accountErr", error: "帐号应为4-16位的数字和字母组成"});
            }

            //密码验证
            if($.trim(attributes.password).length === 0){
                errorArray.push({item:"passwordErr", error: "用户密码不能为空"});
            }
            else if(!passwordReg.test(attributes.password)){
                errorArray.push({item:"passwordErr", error: "密码应为8-30位的字母、数字和特殊字符组成"});
            }

            //电子邮箱验证
            if($.trim(attributes.email).length === 0){
                errorArray.push({item:"emailErr", error: "用户邮箱不能为空"});
            }
            else if(!emailReg.test(attributes.email)){
                errorArray.push({item:"emailErr", error: "电子邮箱的格式不正确"});
            }

            //掌柜手机号验证
            if ($.trim(attributes.mobileNumber).length === 0) {
                errorArray.push({item:"mobileErr", error: "掌柜手机号不能为空"});
            }
            else if (!mobileReg.test(attributes.mobileNumber)) {
                errorArray.push({item:'mobileErr', error:'手机号格式不正确'});
            }


            //短信验证码
            if ($.trim(attributes.mobileCaptcha).length === 0) {
                errorArray.push({item:"captchaErr", error: "验证码不允许为空"});
            }

            //有一个或多个错误, 返回之
            if(errorArray.length > 0){
                return JSON.stringify(errorArray);
            }
        }
    });

    return RegisterInfo;
});