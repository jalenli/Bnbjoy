/**
 * Created by lijizhuang on 16/9/1.
 */
        require.config({
            baseUrl: 'js',
            paths : {
                jquery: 'scripts/jquery/jquery.min',
                underscore: 'scripts/underscore/underscore-min',
                backbone: 'scripts/backbone/backbone-min',
                bootstrap:'scripts/bootstrap/bootstrap.min',
                'bootstrap-dialog':'plugins/bootstrap-dialog.min',
                text: 'plugins/text.min',
                plugins: 'plugins',
                modules: 'modules',
                models: 'models',
                common: 'utils/common',
                url: 'utils/url',
                extensions: 'extensions',
                'jquery.citys': 'plugins/jquery.citys',
                'jquery.showLoading' : 'plugins/jquery.showLoading.min',
                jedate: 'plugins/jedate',
                baidueditor:'plugins/UEditor/ueditor.all',
                bdlanglang:'plugins/UEditor/lang/zh-cn/zh-cn',
                zeroclipboard:'plugins/UEditor/third-party/zeroclipboard/ZeroClipboard',
                ueditorconfig:'plugins/UEditor/ueditor.config',
                'amap': 'http://webapi.amap.com/maps?v=1.3&key=88b78dedee05c5da7ac483d2e5dc8faa&plugin=AMap.Autocomplete,AMap.CitySearch,AMap.Geocoder,AMap.PlaceSearch&callback=init',
            },
            shim : {
                bootstrap : {
                    deps : [ 'jquery' ],
                    exports : 'bootstrap'
                },
                underscore: {
                    exports: '_'
                },
                backbone: {
                    deps: ["underscore", "jquery"],
                    exports: "Backbone"
                },
                'bootstrap-dialog':{
                    deps: ['jquery'],
                    exports: 'BootstrapDialog'
                },
                'jquery.citys':['jquery'],
                'jquery.showLoading':['jquery'],
                baidueditor:{
                    deps: [
                        'ueditorconfig'/*,
                         'text!libs/ueditor/themes/default/css/ueditor.css'*/
                    ],
                    exports: 'UE'
                },

                bdlanglang:{
                    deps: ['baidueditor'],
                    exports: 'bdlanglang'
                }
            }
        });
