/**
 * Created by lijizhuang on 16/9/22.
 */
require(['require.config'], function (Config) {
    require(['jquery', 'underscore', 'backbone', 'common', 'url', 'models/roomTypesMgr/roomType', 'models/roomTypesMgr/roomModel', 'models/roomTypesMgr/roomModels',
             'models/roomTypesMgr/defaultPrice', 'models/roomTypesMgr/roomTypes', 'modules/roomTypesMgr/priceCalendar', 'models/roomTypesMgr/dailyPrices', 'bootstrap-dialog', 'plugins/jquery.showLoading'],
        function ($, _, Backbone, Common, Url, RoomType, RoomModel, RoomModels, DefaultPrice, RoomTypes, PriceCalendarView, DailyPrices, BootstrapDialog) {

            $('.logininfo a:last').click(function (e) {
                e.preventDefault();
                $(this).text("注销中");
                Common.sendAjaxRequest(Url.logout, "", function (data) {
                    window.location.href = "Account/Login";
                }, function (data) {
                    if (data.Content && data.Content.length > 0) {
                        $(this).text("退出");
                    }
                }, false);
            });

            //$('#loading').showLoading();

            Common.sendAjaxRequest(Url.currentBnb, "", function (data) {
                if (data && data.Content && data.Content.length > 0) {
                    var bnb = JSON.parse(data.Content);

                    var fetchRoomTypesParam = {};
                    fetchRoomTypesParam.bnbId = bnb.BnbId;
                    Common.sendAjaxRequest(Url.fetchRoomTypes, fetchRoomTypesParam, function (data) {
                        if (data.Content && data.Content.length > 0) {
                            var result = JSON.parse(data.Content);
                            var roomTypes = new RoomTypes();
                            var index = 0;
                            for (var rt in result.roomTypes) {
                                index++;
                                var rtObj = result.roomTypes[rt].roomType;
                                var roomType = new RoomType();

                                var roomModels = new RoomModels();
                                for (var r in result.roomTypes[rt].rooms) {
                                    var rObj = result.roomTypes[rt].rooms[r];
                                    var roomModel = new RoomModel();
                                    roomModel.set({
                                        rank: rObj.rank,
                                        roomId: rObj.roomId,
                                        roomNumber: rObj.roomNumber,
                                        roomTypeId: rtObj.roomTypeId,
                                        enabled: rObj.enabled,
                                        bnbId: rtObj.bnbId,
                                        roomTypeName: rtObj.roomTypeName
                                    });
                                    roomModels.add(roomModel);
                                }

                                var defaultPrice = new DefaultPrice();
                                var commonPriceObj = result.roomTypes[rt].commonPrice;
                                defaultPrice.set({
                                    roomTypeId: rtObj.roomTypeId,
                                    initPrice: commonPriceObj.initPrice,
                                    mondayPrice: commonPriceObj.mondayPrice,      //周一至周末每天的价格
                                    tuesdayPrice: commonPriceObj.tuesdayPrice,
                                    wednesdayPrice: commonPriceObj.wednesdayPrice,
                                    thursdayPrice: commonPriceObj.thursdayPrice,
                                    fridayPrice: commonPriceObj.fridayPrice,
                                    saturdayPrice: commonPriceObj.saturdayPrice,
                                    sundayPrice: commonPriceObj.sundayPrice
                                });

                                roomType.set({
                                    roomTypeId: rtObj.roomTypeId,  //房型id
                                    roomTypeName: rtObj.roomTypeName, //房型名
                                    roomTypeNotes: rtObj.notes, //房型备注
                                    bnbId: rtObj.bnbId,  //民宿id
                                    rank: rtObj.rank,  //房型排名
                                    sort: index, //房型在当前Bnb的排序 
                                    roomModels: roomModels,  //房间集合
                                    defaultPrice: defaultPrice //价格信息
                                });

                                roomTypes.add(roomType);
                            }

                            var dailyPrices = new DailyPrices();
                            var date = new Date().yyyyMMdd();
                            dailyPrices.syncDailyPrice(date, roomTypes.first().get('roomTypeId'), function (data) {
                                //$('#loading').hideLoading();
                                var priceCalendarView = new PriceCalendarView({ collection: roomTypes, dailyPrices: data });
                                $('.roompriceSetting').append(priceCalendarView.render().el);
                            }, function (data) {
                                //$('#loading').hideLoading();
                                //BootstrapDialog.show({
                                //    title: '系统提示',
                                //    message: data.Content
                                //});
                            });

                        }
                    }, function (data) {
                        if (data.Content && data.Content.length > 0) {
                            $('#loading').hideLoading();
                            BootstrapDialog.show({
                                title: '系统提示',
                                message: data.Content
                            });
                        }
                    }, false);


                    }
            }, function (data) {
                return null;
            }, false);

            
    })
});