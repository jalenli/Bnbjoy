/**
 * Created by lijizhuang on 16/9/25.
 */
define(['jquery', 'underscore', 'backbone', 'common', 'url', 'bootstrap', 'bootstrap-dialog', 'text!modules/roomTypesMgr/modal/changeRoomTypePriceView.html', 'utils/regutil'],
    function ($, _, Backbone, Common, Url, Bootstrap, BootstrapDialog, ChangeRoomTypePriceViewTemplate, RegUtil) {
        var changeRoomTypePriceView = Backbone.View.extend({
            tagName: 'div',
            id: 'changeRoomTypePrice',
            className: 'modal fade',
            template: _.template(ChangeRoomTypePriceViewTemplate),

            initialize: function (attrs) {
                this.options = attrs;
                this.updatingPrice = false;
            },

            events:{
                'click #saveBtn':'onSaveButtonClick'
            },

            onSaveButtonClick: function (e) {
                if (!this.updatingPrice) {
                    var newRoomTypePrice = $.trim($('#newRoomTypePrice').val());

                    var errMsg = "";
                    this.$('.change-roomtype-price-body .errorMsg').html('');

                    if (newRoomTypePrice.length === 0) {
                        //如果房价为空
                        errMsg = "*房价不能为空";
                    }
                    else if (!RegUtil.numberReg.test(newRoomTypePrice)) {
                        //价格格式错误
                        errMsg = "*价格需填写数字";
                    }
                    else if(newRoomTypePrice < 1 || newRoomTypePrice > 9999)
                    {
                        errMsg = "*价格范围1 -> 9999";
                    }

                    if (errMsg.length > 0) {
                        this.$('.change-roomtype-price-body .errorMsg').css('color', 'red');
                        this.$('.change-roomtype-price-body .errorMsg').html(errMsg);
                    }
                    else {
                        //发送请求修改价格
                        this.updatingPrice = true;
                        $(".change-roomtype-price-body #saveBtn").attr('disabled', "true");
                        $(".change-roomtype-price-body #saveBtn").text('提交中..');

                        var param = {};
                        param.roomTypeId = $('.chooseRoomType select').val();
                        param.price = newRoomTypePrice;
                        param.fromDate = _.first(this.options.dateArray);
                        param.toDate = _.last(this.options.dateArray);
                        Common.sendAjaxRequest(Url.modifyDailyPrice, param, _.bind(function (data) {
                            if (data && data.Content && data.Content.length > 0) {
                                //成功后
                                this.$el.modal('hide');

                                //修改模型
                                _.each(this.options.dateArray, _.bind(function (date) {
                                    var targetDailyPrice = this.collection.find(function (d) {
                                        return d.get('date') == date;
                                    });

                                    var value = newRoomTypePrice.indexOf('.') != -1 ?  parseFloat(newRoomTypePrice).toFixed(2) : parseInt(newRoomTypePrice);
                                    targetDailyPrice.set('price', value);
                                }, this));
                                //清空表单
                                $('#newRoomTypePrice').val('');

                                this.updatingPrice = false;
                                $(".change-roomtype-price-body #saveBtn").attr('disabled', "false");
                                $(".change-roomtype-price-body #saveBtn").text('修改');
                            }
                        }, this), _.bind(function (data) {
                            if (data && data.Content && data.Content.length > 0) {
                                this.updatingPrice = false;

                                BootstrapDialog.show({
                                    title: '系统提示',
                                    message: data.Content
                                });

                                $(".change-roomtype-price-body #saveBtn").attr('disabled', "false");
                                $(".change-roomtype-price-body #saveBtn").text('修改');
                            }
                        }, this), false);

                    }
                }
            },

            render:function(){
                this.$el.html(this.template({dateArray:this.options.dateArray}));

                return this;
            }

        });

        return changeRoomTypePriceView;
    });
