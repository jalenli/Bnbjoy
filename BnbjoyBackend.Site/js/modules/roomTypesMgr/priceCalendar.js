/**
 * Created by lijizhuang on 16/9/22.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/roomTypesMgr/priceCalendarView.html', 'modules/roomTypesMgr/toolBar', 'modules/roomTypesMgr/pCalendarInfo', 'extensions/dateExts'],
    function ($, _, Backbone, PriceCalendarViewTemplate, ToolBarView, PCalendarInfoView, DateExts) {
        var priceCalendarView = Backbone.View.extend({
            tagName: 'div',
            template: _.template(PriceCalendarViewTemplate),

            initialize: function (attrs) {
                this.options = attrs;
            },

            events: function () {

            },

            render: function () {
                this.$el.html(this.template());

                var pCalendarInfoView = new PCalendarInfoView({collection: this.options.dailyPrices});
                this.$('.pCalendar').append(pCalendarInfoView.render().el);

                var toolBarView = new ToolBarView({collection: this.collection, dailyPrices: this.options.dailyPrices});
                this.$('.toolbar').append(toolBarView.render().el);

                return this;
            }
        });

        return priceCalendarView;
    });