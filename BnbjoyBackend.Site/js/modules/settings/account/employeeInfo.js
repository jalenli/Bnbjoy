/**
 * Created by lijizhuang on 16/9/15.
 */
define(['jquery', 'underscore', 'backbone', 'common', 'url', 'text!modules/settings/account/employeeInfoView.html', 'modules/settings/account/modal/changePassword',
        'modules/settings/account/modal/changeEmail', 'modules/settings/account/modal/changeMobile', 'modules/settings/account/modal/changeName',
        'modules/settings/account/modal/operateEmployee', 'models/settings/account/bnbPermissionItem', 'models/settings/account/bnbPermissionCollection', 'modules/settings/account/modal/newEmployeePermission', 'bootstrap-dialog'],
    function ($, _, Backbone, Common, Url, EmployeeInfoViewTemplate, ChangePasswordView, ChangeEmailView, ChangeMobileView, ChangeNameView, OperateEmployeeView, BnbPermissionItem, BnbPermissionCollection, NewEmployeePermissionView, BootstrapDialog) {
   var employeeInfoView = Backbone.View.extend({
        tagName:'tr',
        className:'infoDetail',
        template: _.template(EmployeeInfoViewTemplate),

        initialize: function (attrs) {
            this.model.on('change', _.bind(function (model, value){
                this.render();
            }, this));

            this.options = attrs;
        },

        events:{
            "click .rhead a":"onPermissionSave",
            "click .password a":"onChangePassword",
            'click .email a':'onChangeEmail',
            'click .mobile a': 'onChangeMobile',
            'click .employeeName a': 'onChangeName',
            'click .account a' : 'onOperateEmployee',
            'click .addMore': 'onAddPermission'
        },

        onPermissionSave: function (e) {
            var mod = this.model;
            BootstrapDialog.confirm('是否确认修改该权限信息?', _.bind(function(result) {
                if(result) {
                    //获取保存按钮对应的员工id和bnb名称
                    var eid = $(e.currentTarget).data('eid');
                    var bnbid = $(e.currentTarget).data('bnbid');
                    var bnbname = $(e.currentTarget).data('bnbname');
                    var rights = [];
                     $("input[data-eid="+ eid+"][data-bnbname="+ bnbname +"]").each(function(index, obj){
                         rights.push({ key: $(obj).data('key'), value: $(obj).prop('checked') });
                     });

                     var param = {};
                     param.userId = eid;
                     param.bnbId = bnbid;
                     param.permissionStr = JSON.stringify(rights);
                     //发送修改权限的请求
                     Common.sendAjaxRequest(Url.changeEmployeePermission, param, _.bind(function (data) {
                         //保存用户修改的权限
                         _.each(this.model.get('permission'), function (o) {
                             if (o.bnbName === bnbname) {
                                 o.rights = rights; //不以set方式修改属性的,都需要手动渲染
                             }
                         });

                         BootstrapDialog.show({
                             title: '系统提示',
                             message: data.Content
                         });

                         //修改完权限,重新渲染
                         this.render();
                     }, this), _.bind(function (data) {
                         if (data.Content && data.Content.length > 0) {
                             BootstrapDialog.show({
                                 title: '系统提示',
                                 message: data.Content
                             });
                         }
                     }, this), false);

                    // //保存用户修改的权限
                    // _.each(this.model.get('permission'), function (o) {
                    //     if(o.bnbName === bnbname){
                    //         o.rights = rights; //不以set方式修改属性的,都需要手动渲染
                    //     }
                    // });

                    // //修改完权限,重新渲染
                    //this.render();
                }else {
                    //消框
                }
            },this));

        },

        onAddPermission: function (e) {

            var bnbPermissionCollection = new BnbPermissionCollection();

            var param = {};
            param.userId = this.model.get('employeeid');
            Common.sendAjaxRequest(Url.fetchEmployeeBnbPermissions, param,
                _.bind(function (data) {
                    if (data != undefined && data.Content != undefined) {
                        var list = JSON.parse(data.Content);
                        for (var obj in list) {
                            var temp = list[obj];
                            var bnbPermissionItem = new BnbPermissionItem();
                            bnbPermissionItem.set({
                                bnbId: temp.bnbId,
                                bnbName: temp.bnbName,
                                employeeid: temp.userId,
                                enabled: temp.auth,
                            });
                            bnbPermissionCollection.add(bnbPermissionItem);
                        }

                        var addPermissionView = new NewEmployeePermissionView({ collection: bnbPermissionCollection, model: this.model });
                        if ($('body').find('#newEmployeePermission').length === 0) {
                            $('body').append(addPermissionView.render().el);
                        }
                        else {
                            $('#newEmployeePermission').replaceWith(addPermissionView.render().el);
                        }

                        $("#newEmployeePermission").modal({ backdrop: 'static', keyboard: false });
                    }
            },this), _.bind(function (data) {
                if (data != undefined && data.Content != undefined) {
                    BootstrapDialog.show({
                        title: '网络异常',
                        message: '获取民宿权限列表失败'
                    });
                }
            },this), false);

        },

       onChangePassword: function (e) {
           //修改密码弹层
           var changePasswordView = new ChangePasswordView({model: this.model});
           if($('body').find('#changePassword').length === 0) {
               $('body').append(changePasswordView.render().el);
           }
           else{
               $('#changePassword').replaceWith(changePasswordView.render().el);
           }

           $("#changePassword").modal({backdrop: 'static', keyboard: false});
       },

       onChangeEmail: function (e) {
           //修改邮箱弹层
           var changeEmailView = new ChangeEmailView({model: this.model});
           if($('body').find('#changeEmail').length === 0) {
               $('body').append(changeEmailView.render().el);
           }
           else{
               $('#changeEmail').replaceWith(changeEmailView.render().el);
           }

           $("#changeEmail").modal({backdrop: 'static', keyboard: false});
       },

       onChangeMobile: function () {
           var changeMobileView = new ChangeMobileView({model: this.model});
           if($('body').find('#changeMobile').length === 0) {
               $('body').append(changeMobileView.render().el);
           }
           else{
               $('#changeMobile').replaceWith(changeMobileView.render().el);
           }

           $("#changeMobile").modal({backdrop: 'static', keyboard: false});
       },

       onChangeName: function () {
           var changeNameView = new ChangeNameView({model: this.model});
           if($('body').find('#changeName').length === 0) {
               $('body').append(changeNameView.render().el);
           }
           else{
               $('#changeName').replaceWith(changeNameView.render().el);
           }

           $("#changeName").modal({backdrop: 'static', keyboard: false});
       },

       onOperateEmployee: function () {
           var operateEmployeeView = new OperateEmployeeView({model: this.model, collection:this.options.collection});
           if($('body').find('#operateEmployee').length === 0) {
               $('body').append(operateEmployeeView.render().el);
           }
           else{
               $('#operateEmployee').replaceWith(operateEmployeeView.render().el);
           }

           $("#operateEmployee").modal({backdrop: 'static', keyboard: false});
       },

        render: function(){
            this.$el.html(this.template(this.model.attributes));
            return this;
        }
   });

   return employeeInfoView;
});