/**
 * Created by lijizhuang on 16/9/18.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'text!modules/settings/account/modal/operateEmployeeView.html', 'utils/regutil'],
    function ($, _, Backbone, Bootstrap, OperateEmployeeViewTemplate, RegUtil) {
        var operateEmployeeView = Backbone.View.extend({
            tagName: 'div',
            id: 'operateEmployee',
            className: 'modal fade',
            template: _.template(OperateEmployeeViewTemplate),

            initialize: function (attrs) {

                this.options = attrs;
            },

            events:{
                'click #enableEmployee':'onEnableEmployeeClick',
                'click #deleteEmployee':'onDeleteEmployeeClick'
            },

            onEnableEmployeeClick: function () {
                var isEnbaled = this.model.get('enabled');
                this.model.set('enabled', !isEnbaled);

                //发送ajax请求修改状态

                this.$el.modal('hide');

            },

            onDeleteEmployeeClick: function () {
                //发送ajax请求删除员工数据

                //将该model从集合中移除
                this.options.collection.remove(this.model);
                this.$el.modal('hide');
            },

            render: function () {
                this.$el.html(this.template(this.model.attributes));
                return this;
            }
        });

        return operateEmployeeView;
    });