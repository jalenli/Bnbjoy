/**
 * Created by lijizhuang on 16/11/1.
 */
define(['jquery', 'underscore', 'backbone', 'bootstrap', 'common', 'url', 'models/settings/account/bnbPermissionItem', 'models/settings/account/bnbPermissionCollection', 'text!modules/settings/account/modal/newEmployeePermissionView.html'],
    function ($, _, Backbone, Bootstrap, Common, Url, BnbPermissionItem, BnbPermissionCollection, NewEmployeePermissionViewTemplate) {
        var newEmployeePermission = Backbone.View.extend({
            tagName: 'div',
            id: 'newEmployeePermission',
            className: 'modal fade',
            template: _.template(NewEmployeePermissionViewTemplate),

            initialize: function (attrs) {
                this.options = attrs;
            },

            events: {
                'click .new-bnb-permission-body #saveBtn': 'onSaveClick'
            },

            onSaveClick: function (e) {
                //发送请求
                e.preventDefault();
                var userId;
                var list = [];
                $('.new-bnb-permission-item').each(function () {
                    var bnbName = $(this).children(":first").text();
                    var checkBox = $(this).children(":last");
                    if (!userId)
                        userId = checkBox.data('employeeid');

                    var obj = {};
                    obj.bnbId = checkBox.data('bnbid');
                    obj.value = checkBox.is(':checked');
                    list.push(obj);
                });

                var result = {};
                result.userId = userId;
                result.items = list;

                Common.sendAjaxRequest(Url.addRemoveBnbPermissions, result,
                _.bind(function (data) {
                    if (data.Content && data.Content.length > 0) {
                        var employeePermission = JSON.parse(data.Content);
                        var permissionList = [];
                        for (var p in employeePermission) {
                            var item = {};
                            var pObj = employeePermission[p];
                            //var rightsArr = pObj.employeeRights.split(',');
                            var rightsList = [];

                            item.bnbId = pObj.bnbId;
                            item.bnbName = pObj.bnbName;
                            //将权限由数字转成特定键值对列表
                            rightsList = Common.convertPermissionFromStr(pObj.employeeRights);
                            item.rights = rightsList;
                            permissionList.push(item);
                        }
                    }

                    this.options.model.set({
                        permission: permissionList
                    });

                    console.log(this.options.model);
                    this.$el.modal('hide');
                }, this), _.bind(function (data) {
                    if (data != undefined && data.Content != undefined) {
                        this.$('.new-bnb-permission-body .errorMsg').html(data.Content);
                    }
                }, this), false);
            },

            render: function () {
                this.$el.html(this.template({ collection: this.collection, model: this.options.model }));
                return this;
            }
        });

        return newEmployeePermission;
    });