/**
 * Created by lijizhuang on 16/9/9.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/mainTable/rightTableView.html', 'common', 'models/dashboard/roomStatus', 'extensions/dateExts'],
    function ($, _, Backbone, RightTableViewTemplate, Common, RoomStatus, dateExts) {
        var rightTableView = Backbone.View.extend({
            tagName: 'table',
            className: 'rightTable',
            template: _.template(RightTableViewTemplate),

            initialize: function (attrs) {
                _this = this;
                hoverTimeOut = null;
                this.options = attrs;
                contextMenu = null;
                $('body').bind('click', function () {
                    if (Common.objectIsValid(contextMenu))
                        contextMenu.remove();
                });

            },

            events: {
                "mouseenter .bookable": "onBookableHover",
                "mouseleave .bookable": "onBookableBlur",
                "click .bookable": "onBookableClick",
                "click .booked": "onBookedClick",
                "click .checkedin": "onCheckedInClick",
                "click .away": "onAwayClick",
                "click .locked": "onLockedClick"
            },

            onBookableHover: function (ev) {
                var elmTd = $(ev.currentTarget), elmInfo = elmTd.find('.bookableInfo');
                if (elmInfo.length) {
                    if (!hoverTimeOut) {
                        hoverTimeOut = setTimeout(function () {
                            elmInfo.fadeIn("fast", 'linear');
                            var yIndex = elmTd.data('y');

                            $('.roomcount').eq(yIndex).css({ 'background': '#fff0c4' });
                            //console.log(Common.rgbToHex($('.roomcount').eq(yIndex).css('backgroundColor')));
                        }, 50);
                    }
                }

            },

            onBookableBlur: function (ev) {
                var elmTd = $(ev.currentTarget), elmInfo = elmTd.find('.bookableInfo');
                if (elmInfo.length) {
                    clearTimeout(hoverTimeOut);
                    hoverTimeOut = null;
                    //elmInfo.fadeOut("fast", 'linear');
                    elmInfo.hide();
                    var yIndex = elmTd.data('y');
                    $('.roomcount').eq(yIndex).css({ 'background': 'lightgray' });
                }
            },

            onBookableClick: function (e) {
                e.preventDefault();
                e.stopPropagation();

                var targetDate = new Date($(e.currentTarget).data('date'));
                var today = new Date();

                var xx = (e.pageX + 5) || 0,
                    yy = (e.pageY + 25) || 0;

                var width = 105, height = 123;

                //防止菜单溢出窗口
                var maxRight = e.pageX + width;
                if (maxRight > document.documentElement.clientWidth) {
                    xx = (xx - width - 5) || 0;
                }

                var maxBottom = e.pageY + height;
                if (maxBottom > document.documentElement.clientHeight) {
                    yy = (yy - height - 5) || 0;
                }

                if (Common.objectIsValid(contextMenu))
                    contextMenu.remove();

                if (Common.dateIsEqual(targetDate, new Date())) {
                    //同一天,可办理预定和直接入住
                    require(['modules/dashboard/contextMenu/bookAndCheckinMenu'], function (BookAndCheckinMenuView) {
                        contextMenu = new BookAndCheckinMenuView({ model: { top: yy, left: xx } });
                        $('body').append(contextMenu.render().el);
                        contextMenu.showView();
                    });

                    return;
                }

                if (Common.dateIsMoreThan(targetDate, today)) {
                    //将来,可预定
                    require(['modules/dashboard/contextMenu/bookableMenu'], _.bind(function (BookableMenuView) {

                        //默认toDate等于fromDate + 1
                        var cr = $(e.currentTarget).parent();
                        var param = {};
                        param.roomTypeId = cr.data('roomtypeid');
                        param.roomType = cr.data('roomtype');
                        param.roomId = cr.data('roomid');
                        param.roomNumber = cr.data('roomnumber');
                        param.fromDate = $(e.currentTarget).data('date');
                        param.bnbId = this.options.bnbId;
                        var tempFromDate = new Date(param.fromDate.replace(/-/g, "/"));
                        param.toDate = tempFromDate.tomorrow().yyyyMMdd();
                        param.top = yy;
                        param.left = xx;
                        contextMenu = new BookableMenuView({ model: param, collection: this.collection });
                        $('body').append(contextMenu.render().el);
                        contextMenu.showView();
                    }, this));

                    return;
                }

                if (Common.dateIsLessThan(targetDate, today)) {
                    //过去,可补录
                    require(['modules/dashboard/contextMenu/makeupMenu'], function (MakeupMenuView) {
                        contextMenu = new MakeupMenuView({ model: { top: e.pageY, left: e.pageX } });
                        $('body').append(contextMenu.render().el);
                        contextMenu.showView();
                    });

                    return;
                }
            },

            onBookedClick: function (e) {
                e.preventDefault();
                e.stopPropagation();

                var targetDate = new Date($(e.currentTarget).data('date'));
                var today = new Date();

                var xx = (e.pageX + 5) || 0,
                    yy = (e.pageY + 25) || 0;

                var width = 105, height = 123;

                //防止菜单溢出窗口
                var maxRight = e.pageX + width;
                if (maxRight > document.documentElement.clientWidth) {
                    xx = (xx - width - 5) || 0;
                }

                var maxBottom = e.pageY + height;


                if (Common.objectIsValid(contextMenu))
                    contextMenu.remove();

                if (Common.dateIsEqual(targetDate, new Date())) {
                    if (maxBottom > document.documentElement.clientHeight) {
                        yy = (yy - height - 5) || 0;
                    }

                    //同一天,已预定的话可以直接办理入住
                    require(['modules/dashboard/contextMenu/canCheckinMenu'], function (CanCheckinMenuView) {
                        contextMenu = new CanCheckinMenuView({ model: { top: yy, left: xx } });
                        $('body').append(contextMenu.render().el);
                        contextMenu.showView();
                    });
                    return;
                }

                if (Common.dateIsMoreThan(targetDate, new Date())) {
                    height = 80;
                    if (maxBottom > document.documentElement.clientHeight) {
                        yy = (yy - height - 5) || 0;
                    }

                    //将来,可取消
                    require(['modules/dashboard/contextMenu/bookedMenu'], function (BookedMenuView) {
                        contextMenu = new BookedMenuView({ model: { top: yy, left: xx } });
                        $('body').append(contextMenu.render().el);
                        contextMenu.showView();
                    });
                    return;
                }

            },

            onCheckedInClick: function (e) {
                e.preventDefault();
                e.stopPropagation();

                var targetDate = new Date($(e.currentTarget).data('date'));
                var today = new Date();

                var xx = (e.pageX + 5) || 0,
                    yy = (e.pageY + 25) || 0;

                var width = 105, height = 123;

                //防止菜单溢出窗口
                var maxRight = e.pageX + width;
                if (maxRight > document.documentElement.clientWidth) {
                    xx = (xx - width - 5) || 0;
                }

                var maxBottom = e.pageY + height;
                if (maxBottom > document.documentElement.clientHeight) {
                    yy = (yy - height - 5) || 0;
                }

                if (Common.objectIsValid(contextMenu))
                    contextMenu.remove();

                require(['modules/dashboard/contextMenu/checkedinMenu'], function (CheckedinMenuView) {
                    contextMenu = new CheckedinMenuView({ model: { top: yy, left: xx } });
                    $('body').append(contextMenu.render().el);
                    contextMenu.showView();
                });
            },

            onAwayClick: function (e) {
                e.preventDefault();
                e.stopPropagation();

                var targetDate = new Date($(e.currentTarget).data('date'));
                var today = new Date();

                var xx = (e.pageX + 5) || 0,
                    yy = (e.pageY + 25) || 0;

                var width = 105, height = 80;

                //防止菜单溢出窗口
                var maxRight = e.pageX + width;
                if (maxRight > document.documentElement.clientWidth) {
                    xx = (xx - width - 5) || 0;
                }

                var maxBottom = e.pageY + height;
                if (maxBottom > document.documentElement.clientHeight) {
                    yy = (yy - height - 5) || 0;
                }

                if (Common.objectIsValid(contextMenu))
                    contextMenu.remove();

                require(['modules/dashboard/contextMenu/awayMenu'], function (AwayMenuView) {
                    contextMenu = new AwayMenuView({ model: { top: yy, left: xx } });
                    $('body').append(contextMenu.render().el);
                    contextMenu.showView();
                });
            },

            onLockedClick: function (e) {
                e.preventDefault();
                e.stopPropagation();

                var targetDate = new Date($(e.currentTarget).data('date'));
                var today = new Date();

                var xx = (e.pageX + 5) || 0,
                    yy = (e.pageY + 25) || 0;

                var width = 105, height = 80;

                //防止菜单溢出窗口
                var maxRight = e.pageX + width;
                if (maxRight > document.documentElement.clientWidth) {
                    xx = (xx - width - 5) || 0;
                }

                var maxBottom = e.pageY + height;
                if (maxBottom > document.documentElement.clientHeight) {
                    yy = (yy - height - 5) || 0;
                }

                if (Common.objectIsValid(contextMenu))
                    contextMenu.remove();

                require(['modules/dashboard/contextMenu/unlockMenu'], function (UnlockMenuView) {
                    contextMenu = new UnlockMenuView({ model: { top: yy, left: xx } });
                    $('body').append(contextMenu.render().el);
                    contextMenu.showView();
                });
            },

            render: function () {
                var content = this.template({ value: this.collection, dateStr: this.options.dateStr });
                $(this.el).html(content);

                //var fromDate = new Date(this.options.dateStr);
                //var today = new Date();

                //this.collection.each(function (roomInfo) {
                //    for(var i=0; i<30; i++) {
                //        var indexDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() + i);
                //        var orders = roomInfo.get('orders');
                //        if(Common.objectIsValid(orders)) {
                //            var order = orders.find(function (o) {
                //                var fromDate = new Date(o.get('fromDate'));//获取订单入住时间,不含离店天
                //                var toDate = new Date(o.get('toDate'));
                //                return Common.checkDayIsInRange(fromDate, toDate, indexDate); //检测该天是否有订单
                //            });
                //        }
                //
                //        if(Common.objectIsValid(order)){
                //            //有订单
                //            if(order.roomStatus === RoomStatus.Booked){
                //                //已预定
                //            }
                //            else if(order.roomStatus == RoomStatus.CanCheckIn){
                //                //可入住
                //            }
                //            else if(order.roomStatus === RoomStatus.CheckedIn){
                //                //已入住
                //            }
                //            else if(order.roomStatus == RoomStatus.Locked){
                //                //已关闭
                //            }
                //
                //            console.log(indexDate + '---' + order.get('orderId'));
                //        }
                //        else{
                //
                //            if(indexDate < today){
                //                //可补录
                //            }
                //            else if(Common.dateIsEqual(indexDate, today)){
                //                //可预定/可入住
                //            }
                //            else if(indexDate > today){
                //                //可预定
                //            }
                //        }
                //
                //
                //        console.log(roomInfo);
                //    }
                //});

                return this;
            }
        });

        return rightTableView;
    });