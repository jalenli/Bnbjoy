/**
 * Created by lijizhuang on 16/9/8.
 */
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/mainTable/theaderCellView.html'],
  function ($, _, Backbone, TheaderCellViewTemplate) {
     var theaderCellView = Backbone.View.extend({
          tagName:'th',
          className:'roomcount',
          template: _.template(TheaderCellViewTemplate),

          initialize: function () {

          },

          events:{

          },

          render: function(){
              $(this.el).html(this.template(this.model.attributes));
              return this;
          }
     });

    return theaderCellView;
  }
);