/**
 * Created by lijizhuang on 16/9/8.
 */
define(['jquery','underscore', 'backbone', 'text!modules/dashboard/mainTable/theaderView.html', 'modules/dashboard/mainTable/theaderCell'],
    function ($, _, Backbone, TheaderViewTemplate, TheaderCell) {
    var mainTableView = Backbone.View.extend({
        tagName: 'table',
        className: 'timeAxis',
        template: _.template(TheaderViewTemplate),

        initialize: function () {
            _this = this;
        },

        events:{

        },

        render: function () {
            var collection = this.collection;

            $(this.el).html(this.template());
            $tr = this.$('tbody tr').first();
            collection.each(function (bookableRoom) {
                var cellView = new TheaderCell({model: bookableRoom});
                var cellViewDom = cellView.render().el;
                //console.log(cellViewDom);
                $tr.append(cellViewDom);
            });

            return this;
        }
    });

    return mainTableView;
});