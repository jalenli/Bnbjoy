/**
 * Created by lijizhuang on 16/9/7.
 */
define(['jquery', 'underscore', 'backbone', 'models/dashboard/roomInfos', 'text!modules/dashboard/mainTable/dashTableView.html',
        'modules/dashboard/mainTable/leftTable', 'modules/dashboard/mainTable/rightTable'],
    function ($, _, Backbone, RoomInfos, DashTableViewTemplate, LeftTableView, RightTableView) {
        var dashTableView = Backbone.View.extend({
            tagName: 'div',
            template: _.template(DashTableViewTemplate),

            initialize: function (attrs) {
                _this = this;
                this.options = attrs;

                this.collection.on({
                    'add': _.bind(function (model, collection, options) {
                        this.render();
                    }, this),
                    'remove': _.bind(function (model, collection, options) {
                        this.render();
                    }, this),
                    'change': _.bind(function (model, collection, options) {
                        this.render();
                    }, this)
                });
            },

            events: {
            },

            render: function () {
                //添加leftTable内容
                this.$el.html(this.template);

                var leftTableView = new LeftTableView({ collection: this.collection });
                var leftContent = leftTableView.render().el;
                this.$('.leftSide').append(leftContent);

                //添加rightTable内容
                var rightTableView = new RightTableView({ collection: this.collection, dateStr: this.options.dateStr, bnbId: this.options.bnbId });
                var rightContent = rightTableView.render().el;
                this.$('.rightSide').append(rightContent);

                this.$(".rightSide").bind("scroll", function () {
                    //用动画方式滚动
                    //$('.availableInfo').animate({
                    //        scrollLeft: $(this).scrollLeft()
                    //    },0);

                    //直接设定位置
                    $('.availableInfo').scrollLeft($(this).prop("scrollLeft")); //左右滚动带动头部表格
                    $('.leftSide').scrollTop($(this).prop("scrollTop"));  //上下滚动带动左侧表格
                });

                return this;
            }
        });

        return dashTableView;
    });