/**
 * Created by lijizhuang on 16/9/12.
 */

//可预定/可直接入住
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/bookAndCheckinMenuView.html'], function ($, _, Backbone, BookAndCheckinMenuViewTemplate) {
    var bookAndCheckMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'bookAndCheckMenu',
        className: 'contextMenu',

        template: _.template(BookAndCheckinMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #newOrder':'onCreateOrder',
            'click #handleCheckin':'onHandleCheckin',
            'click #lockRoom': 'onLockRoom'
        },

        onCreateOrder: function () {
            console.log('新建预定');
            _this.removeView();
        },

        onHandleCheckin: function () {
            console.log('办理入住');
            _this.removeView();
        },

        onLockRoom: function () {
            console.log('关闭房间');
            _this.removeView();
        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return bookAndCheckMenuView;
});