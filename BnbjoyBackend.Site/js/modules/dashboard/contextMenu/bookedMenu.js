/**
 * Created by lijizhuang on 16/9/3.
 */
//已预定

define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/bookedMenuView.html'], function ($, _, Backbone, BookedMenuViewTemplate) {
    var bookedMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'bookedMenu',
        className: 'contextMenu',

        template: _.template(BookedMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #viewOrder':'onViewOrder',
            'click #cancelOrder' : 'onCancelOrder'
        },

        onViewOrder: function () {
            console.log('查看订单');
            this.removeView();
        },

        onCancelOrder: function () {
            console.log('取消订单');
            this.removeView();
        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return bookedMenuView;
});