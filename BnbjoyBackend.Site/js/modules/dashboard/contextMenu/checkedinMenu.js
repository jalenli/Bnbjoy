/**
 * Created by lijizhuang on 16/9/3.
 */
//已入住
define(['jquery', 'underscore', 'backbone', 'text!modules/dashboard/contextMenu/CheckedinMenuView.html'], function ($, _, Backbone, CheckiednMenuViewTemplate) {
    var checkedinMenuView = Backbone.View.extend({
        tagName:'ul',
        id: 'checkedinMenu',
        className: 'contextMenu',

        template: _.template(CheckiednMenuViewTemplate),

        initialize: function () {
            _this = this;
        },

        events: {
            'click #viewOrder':'onViewOrder',
            'click #leftCalc' : 'onLeftCalc',
            'click #cancelOrder': 'onCancelOrder'
        },

        onViewOrder: function () {
            console.log('查看订单');
            this.removeView();
        },

        onLeftCalc: function () {
            console.log('离店结算');
            this.removeView();
        },

        onCancelOrder: function () {
            console.log('取消订单');
            this.removeView();
        },

        render: function () {
            var model = this.model;
            $(this.el).html(this.template(model));
            return this;
        },

        showView: function () {
            $(_this.el).show().css({
                'top': _this.model.top + 'px',
                'left': _this.model.left + 'px'
            });
        },

        removeView:function(){
            $(_this.el).remove();
        }
    });

    return checkedinMenuView;
});