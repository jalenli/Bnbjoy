﻿/**
 * Created by lijizhuang on 16/10/16.
 */
define([], function () {
    var host = location.host;
    var vDir = "BnbjoyBackend.Site";

    function makeUrl(service) {
        return '//' + host + '/' + vDir + '/' + (service ? service : '');
    }

    return {
        mobileCaptcha: makeUrl("account/sendMobileCaptcha"),
        register: makeUrl("account/register"),
        login: makeUrl("account/login"),
        logout: makeUrl("account/logout"),
        ownerandemployees: makeUrl("settings/OwnerAndEmployeesInfo"),
        changeEmployeePermission: makeUrl("settings/ChangeEmployeePermission"),
        createNewEmployee: makeUrl("settings/CreateNewEmployee"),
        fetchEmployeeBnbPermissions: makeUrl("settings/FetchEmployeeBnbPermissions"),
        addRemoveBnbPermissions: makeUrl("settings/AddRemoveBnbPermissions"),
        newRoomType: makeUrl("roomtype/NewRoomType"),
        currentBnb: makeUrl("roomtype/CurrentBnb"),
        fetchRoomTypes: makeUrl("roomtype/FetchRoomTypes"),
        moveUpRoomTypeRank: makeUrl("roomtype/MoveUpRoomTypeRank"),
        moveDownRoomTypeRank: makeUrl("roomtype/MoveDownRoomTypeRank"),
        modifyDailyPrice: makeUrl("roomtype/ModifyDailyPrice"),
        listDailyPrice: makeUrl("roomtype/ListDailyPrice"),
        listCustomDailyPrice: makeUrl("roomtype/ListCustomDailyPrice"),
        newOrder: makeUrl("home/NewOrder"),
        availableRooms: makeUrl("home/AvailableRooms"),
    }
});