/**
 * Created by lijizhuang on 16/9/16.
 */
define([], function () {

    return {
        accountReg: /^([\u4e00-\u9fa5]{2,8}|[0-9a-zA-Z_]{4,16})$/,
        passwordReg : new RegExp('(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}'),
        emailReg : /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/,
        mobileReg: /^1[3|4|5|7|8]\d{9}$/,
        chineseNameReg: /^[\u4e00-\u9fa5 ]{2,20}$/,
        numberReg: /^[0-9.]+$/,
        notNullReg: /^\s+$/,
    };
});