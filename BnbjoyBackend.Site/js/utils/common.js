/**
 * Created by lijizhuang on 16/9/7.
 */
define(['jquery', 'bootstrap-dialog', 'plugins/jquery.showLoading'], function ($, BootstrapDialog) {
    function festivalCheck(month, day, dayOfWeek) {
        var str = '';
        switch (month + '.' + day) {
            case "1.1": str = "元旦"; break;
            case "2.14": str = "情人"; break;
            case "3.8": str = "妇女"; break;
            case "5.1": str = "劳动"; break;
            case "6.1": str = "儿童"; break;
            case "8.1": str = "建军"; break;
            case "9.10": str = "教师"; break;
            case "10.1": str = "国庆"; break;
            case "12.24": str = "平安"; break;
            case "12.25": str = "圣诞"; break;
            default: str = dayOfWeek; break;
        }
        return str;
    }

    function getDayOfWeek(dateStr) {
        var weekday = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
        var day = new Date(dateStr).getDay();
        return weekday[day];
    }

    function checkDayIsInRange(dateFrom, dateTo, dateCheck) {
        return dateCheck >= dateFrom && dateCheck <= dateTo;
    }

    function objectIsValid(obj) {
        if (obj == null || obj == undefined || !obj) {
            return false;
        }

        return true;
    }

    //比较左边日期是否等于右边日期, 精确到天
    function dateIsEqual(dateA, dateB) {
        return dateA.getFullYear() == dateB.getFullYear() && dateA.getMonth() == dateB.getMonth() && dateA.getDate() == dateB.getDate();
    }

    //比较左边日期是否大于右边日期, 精确到天
    function dateIsMoreThan(dateA, dateB) {
        return dateA.getFullYear() > dateB.getFullYear() || dateA.getMonth() > dateB.getMonth() || dateA.getDate() > dateB.getDate();
    }

    //比较左边日期是否小于右边日期, 精确到天
    function dateIsLessThan(dateA, dateB) {
        return dateA.getFullYear() < dateB.getFullYear() || dateA.getMonth() < dateB.getMonth() || dateA.getDate() < dateB.getDate();
    }

    function rgbToHex(colorval) {
        var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        delete (parts[0]);
        for (var i = 1; i <= 3; ++i) {
            parts[i] = parseInt(parts[i]).toString(16);
            if (parts[i].length == 1) parts[i] = '0' + parts[i];
        }
        color = '#' + parts.join('');

        return color;
    }

    function getRandomFloat(min, max) {
        return Math.random() * (max - min) + min;
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function isIdCard(idcard) {
        var Errors = new Array(true, false, false, false, false); 
        var area = {
            11: "北京",        12: "天津",        13: "河北",        14: "山西",        15: "内蒙古",
            21: "辽宁",        22: "吉林",        23: "黑龙江",        31: "上海",        32: "江苏",
            33: "浙江",        34: "安徽",        35: "福建",        36: "江西",        37: "山东",
            41: "河南",        42: "湖北",        43: "湖南",        44: "广东",        45: "广西",
            46: "海南",        50: "重庆",        51: "四川",        52: "贵州",        53: "云南",
            54: "西藏",        61: "陕西",        62: "甘肃",        63: "青海",        64: "宁夏",
            65: "xinjiang",        71: "台湾",        81: "香港",        82: "澳门",        91: "国外"
        };
        var Y, JYM;
        var S, M;
        var idcard_array = [];
        idcard_array = idcard.split("");
        if (area[parseInt(idcard.substr(0, 2))] === null) return Errors[4];
        switch (idcard.length) {
            case 18:
                if (parseInt(idcard.substr(6, 4)) % 4 === 0 || (parseInt(idcard.substr(6, 4)) % 100 === 0 && parseInt(idcard.substr(6, 4)) % 4 === 0)) {
                    ereg = /^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$/; //闰年出生日期的合法性正则表达式
                } else {
                    ereg = /^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$/; //平年出生日期的合法性正则表达式
                }
                if (ereg.test(idcard)) {
                    S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7 + (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9 + (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10 + (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5 + (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8 + (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4 + (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2 + parseInt(idcard_array[7]) * 1 + parseInt(idcard_array[8]) * 6 + parseInt(idcard_array[9]) * 3;
                    Y = S % 11;
                    M = "F";
                    JYM = "10X98765432";
                    M = JYM.substr(Y, 1);
                    if (M.toUpperCase() == idcard_array[17].toUpperCase())
                        return Errors[0];
                    else
                        return Errors[3];
                } else
                    return Errors[2];
                break;
            default:
                return Errors[1];
        }
    }

    function sendAjaxRequest(url, params, successCallback, errorCallback, showLoading, needDelay, popErrorDialog) {
        popErrorDialog = popErrorDialog == undefined ? false : popErrorDialog; //有错误是是否自动弹出
        needDelay = needDelay == undefined ? true : needDelay;  //是否延迟出现进度圈，避免闪屏，默认100毫秒

        var hide = false;
        loadingEle = $('#loading');

        if (showLoading) {
            hide = false;

            if (needDelay) {
                setTimeout(function () {
                    if (!hide) {
                        loadingEle.showLoading(); //显示loading
                    }
                }, 100);
            }
            else {
                if (!hide) {
                    loadingEle.showLoading(); //显示loading
                }
            }
        }

        $.ajax({
            url: url,
            type: 'post',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(params),
            async: true,
            success: function (data) {
                if (data.StatusCode == 302)
                {
                    document.location = data.Content;
                }
                else if (data.StatusCode != 200) {
                    this.error(data);
                }
                else {
                    successCallback && (typeof successCallback === 'function') && successCallback(data);
                }
            },
            error: function (err) {
                if (popErrorDialog) {
                    BootstrapDialog.show({
                        title: '系统提示',
                        message: err.Content
                    });
                }
                errorCallback && (typeof errorCallback === 'function') && errorCallback(err);
            },
            complete: function (XMLHttpRequest, textStatus) {
                if (showLoading) {
                    hide = true;
                    if(loadingEle.length > 0)
                        loadingEle.hideLoading(); //隐藏loading
                }
            }
        });

    }

    function convertPermissionFromStr(rightsStr){
        var rightsList = [];
        if (rightsStr) {
            if (rightsStr.indexOf("1") > -1) {
                rightsList.push({ key: 'dashmgmt', value: true });
            }
            else {
                rightsList.push({ key: 'dashmgmt', value: false });
            }

            if (rightsStr.indexOf("2") > -1) {
                rightsList.push({ key: 'ordermgmt', value: true });
            }
            else {
                rightsList.push({ key: 'ordermgmt', value: false });
            }

            if (rightsStr.indexOf("3") > -1) {
                rightsList.push({ key: 'infomgmt', value: true });
            }
            else {
                rightsList.push({ key: 'infomgmt', value: false });
            }

            if (rightsStr.indexOf("4") > -1) {
                rightsList.push({ key: 'roommgmt', value: true });
            }
            else {
                rightsList.push({ key: 'roommgmt', value: false });
            }

            if (rightsStr.indexOf("5") > -1) {
                rightsList.push({ key: 'socialmgmt', value: true });
            }
            else {
                rightsList.push({ key: 'socialmgmt', value: false });
            }

            if (rightsStr.indexOf("6") > -1) {
                rightsList.push({ key: 'customermgmt', value: true });
            }
            else {
                rightsList.push({ key: 'customermgmt', value: false });
            }

            if (rightsStr.indexOf("7") > -1) {
                rightsList.push({ key: 'statsmgmt', value: true });
            }
            else {
                rightsList.push({ key: 'statsmgmt', value: false });
            }

            if (rightsStr.indexOf("8") > -1) {
                rightsList.push({ key: 'settingsmgmt', value: true });
            }
            else {
                rightsList.push({ key: 'settingsmgmt', value: false });
            }
        }

        return rightsList;
    }

    return {
        festivalCheck: festivalCheck,
        getDayOfWeek: getDayOfWeek,
        checkDayIsInRange: checkDayIsInRange,
        objectIsValid: objectIsValid,
        dateIsEqual: dateIsEqual,
        rgbToHex: rgbToHex,
        dateIsMoreThan: dateIsMoreThan,
        dateIsLessThan: dateIsLessThan,
        getRandomFloat: getRandomFloat,
        getRandomInt: getRandomInt,
        sendAjaxRequest: sendAjaxRequest,
        convertPermissionFromStr: convertPermissionFromStr,
        isIdCard: isIdCard,
    }
});