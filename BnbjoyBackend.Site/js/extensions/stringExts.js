/**
 * Created by lijizhuang on 16/9/28.
 */
define(function() {
    //得到含中英文字符串的长度
    String.prototype.gbLength = function() {
        var len = 0;
        for (var i=0; i<this.length; i++) {
            if (this.charCodeAt(i)>127 || this.charCodeAt(i)==94) {
                len += 2;
            } else {
                len ++;
            }
        }
        return len;
    };

    return String;
});