﻿using Bnbjoy.Business.Model.Dashboard;
using BnbjoyBackend.Site.ConstantsNS;
using BnbjoyBackend.Site.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BnbjoyBackend.Site.Controllers
{
    [BnbjoyAuthorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            this.ViewBag.Nav = Constants.DashboardManagement;
            return View();
        }

        /// <summary>
        /// 新建订单
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> NewOrder(NewOrderModel model)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string orderJson = JsonConvert.SerializeObject(model);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "order/new_dash_order", orderJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的新建订单请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，订单新建失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> AvailableRooms(QueryAvailableRoomsParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var accessToken = AuthorizedUserManager.Instance.AccessToken;
                    string paramJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(accessToken, "order/fetch_available_rooms", paramJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "获取房间数服务失败"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，查询房间数失败"));
            }
        }

    }
}