﻿using Bnbjoy.Business.Model.Account;
using BnbjoyBackend.Site.ConstantsNS;
using BnbjoyBackend.Site.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BnbjoyBackend.Site.Controllers
{

    [BnbjoyAuthorize]
    public class SettingsController : BaseController
    {
        // GET: Settings
        public ActionResult Index()
        {
            //跳转至settings/account
            return RedirectToAction("Account", "Settings");
        }

        public ActionResult Account() 
        {
            this.ViewBag.Nav = Constants.SettingsManagement;
            this.ViewBag.SubNav = Constants.AccountManagement;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateNewEmployee(NewEmployeeParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    string paramJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(AuthorizedUserManager.Instance.AccessToken, "account/new_employee", paramJson);
                    return Json(response);
                }
                else 
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的新建请求"));
                }
            }
            catch (Exception ex) 
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，新建员工账户失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> OwnerAndEmployeesInfo() 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    HttpPostResponse response = await ServiceManager.Instance.PostService(AuthorizedUserManager.Instance.AccessToken, "account/ownerandemployees", "");
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的注册请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，注册失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> ChangeEmployeePermission(ChangeEmployeePermissionParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    string paramJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(AuthorizedUserManager.Instance.AccessToken, "account/change_employee_permissions", paramJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的权限修改请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，权限修改失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> FetchEmployeeBnbPermissions(EmployeeBnbPermissionRequestParam param)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    string paramJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(AuthorizedUserManager.Instance.AccessToken, "account/employee_bnb_permissions", paramJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的权限列表获取请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，权限列表获取失败"));
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddRemoveBnbPermissions(AddRemoveBnbPermissionParam param) 
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    string paramJson = JsonConvert.SerializeObject(param);
                    HttpPostResponse response = await ServiceManager.Instance.PostService(AuthorizedUserManager.Instance.AccessToken, "account/add_remove_bnb_permission", paramJson);
                    return Json(response);
                }
                else
                {
                    return Json(new HttpPostResponse(HttpStatusCode.Forbidden, "无效的权限列表获取请求"));
                }
            }
            catch (Exception ex)
            {
                return Json(new HttpPostResponse(HttpStatusCode.InternalServerError, "服务错误，权限列表获取失败"));
            }
        }

        public ActionResult Payment()
        {
            this.ViewBag.Nav = Constants.SettingsManagement;
            this.ViewBag.SubNav = Constants.PaymentManagement;
            return View();
        }

        public ActionResult Personal()
        {
            this.ViewBag.Nav = Constants.SettingsManagement;
            this.ViewBag.SubNav = Constants.PersonalManagement;
            return View();
        }

    }
}