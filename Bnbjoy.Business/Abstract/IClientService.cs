﻿using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IClientService : IDisposable
    {
        Task<bool> HasClient();

        Task<Client> AddClient(Client client);

        Task<IEnumerable<Client>> AddClients(IEnumerable<Client> clients);

        Task<Client> FindClient(string Id);
    }
}
