﻿using Bnbjoy.Business.Model.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IDashboardService : IDisposable
    {
        Task<HeadInfoResponse> DashHeadInfo(string userId);
    }
}
