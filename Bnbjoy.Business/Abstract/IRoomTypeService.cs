﻿using Bnbjoy.Business.Model.Dashboard;
using Bnbjoy.Business.Model.RoomType;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IRoomTypeService : IDisposable
    {
        Task<RoomTypeResponse> NewRoomType(RoomType roomType, IEnumerable<Room> rooms, CommonPrice commonPrice);

        Task<QueryAllRoomTypesResponse> Catalog(string bnbId);

        Task<bool> MoveUpRank(string bnbId, int currentRank);

        Task<bool> MoveDownRank(string bnbId, int currentRank);

        Task<bool> ModifyDailyPrice(DateTime fromDate, DateTime toDate, string roomTypeId, decimal price);

        /// <summary>
        /// 根据roomtype id获特定月份的每日房型价格
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="roomTypeId"></param>
        /// <returns></returns>
        Task<SortedDictionary<int, decimal>> FetchRoomTypeDailyPrice(int year, int month, string roomTypeId);

        /// <summary>
        /// 根据roomtype id获取房型每日价格
        /// </summary>
        /// <param name="fromTime"></param>
        /// <param name="toTime"></param>
        /// <param name="roomTypeId"></param>
        /// <returns></returns>
        Task<SortedDictionary<string, decimal>> FetchCustomRoomTypeDailyPrice(DateTime fromTime, DateTime toTime, string roomTypeId);

        /// <summary>
        /// 根据bnb id获取所有房型每日的价格
        /// </summary>
        /// <param name="fromTime"></param>
        /// <param name="toTime"></param>
        /// <param name="bnbId"></param>
        /// <returns></returns>
        Task<Dictionary<string, SortedDictionary<string, decimal>>> FetchAllRoomTypeDailyPrice(DateTime fromTime, DateTime toTime, string bnbId);

        /// <summary>
        /// 根据bnb id获取所有房间的基本信息，显示在dashboard
        /// </summary>
        /// <param name="fromTime"></param>
        /// <param name="toTime"></param>
        /// <param name="bnbId"></param>
        /// <returns></returns>
        Task<List<RoomsBasicInfoModel>> FetchAllBasicRoomsInfo(DateTime fromTime, DateTime toTime, string bnbId);
    }
}
