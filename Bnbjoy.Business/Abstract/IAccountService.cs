﻿using Bnbjoy.Business.Model.Account;
using Bnbjoy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IAccountService : IDisposable
    {
        Task<bool> AddRole(Role role);

        Task<bool> AddPermission(Permission permission);

        Task<bool> AddUser(User user, Profile profile);

        Task<bool> AddProfile(Profile profile);

        Task<User> FindUser(string mobileNumber);

        Task<User> FindUserByEmail(string email);

        Task<User> FindUser(string userName, string password);

        Task<User> ReadUserInfo(string userName);

        Task<dynamic> ReadUserAndPermission(string userId, string role, string bnbId);

        Task<IEnumerable<dynamic>> FetchBA_Bnbs(string userName);

        Task<IEnumerable<dynamic>> FetchBE_Bnbs(string userName);

        Task<OwnerAndEmployeeModel> ReadUserDetailInfo(string userName);

        Task<IEnumerable<EmployeeModel>> ReadRelatedEmployeesInfo(string ownerId);

        Task<bool> ChangePermission(string userId, string bnbId, string rightsStr);

        Task<IEnumerable<EmployeeBnbPermissionModel>> ReadEmployeeBnbPermissionList(string userId);

        Task<IEnumerable<dynamic>> AddRemoveBnbPermission(AddRemoveBnbPermissionParam param);
    }
}
