﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Abstract
{
    public interface IMobileCaptchaService : IDisposable
    {
        Task<bool> SendMobileCaptcha(string mobileNum);

        Task<bool> RemoveMobileCaptcha(string mobileNum);

        Task<bool> VerifyMobileCaptcha(string mobileNum, string vCode);
    }
}
