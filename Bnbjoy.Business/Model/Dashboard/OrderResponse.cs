﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class OrderResponse
    {
        [JsonProperty("order")]
        public Bnbjoy.Domain.Entities.Order order { get; set; }

        [JsonProperty("customers")]
        public IEnumerable<Bnbjoy.Domain.Entities.Customer> customers { get; set; }

        [JsonProperty("spendings")]
        public IEnumerable<Bnbjoy.Domain.Entities.OrderSpending> spendings { get; set; }
    }
}
