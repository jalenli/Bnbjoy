﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class NewCustomerModel
    {
        [JsonProperty("customerName")]
        [Required(ErrorMessage = "入住人姓名不能为空")]
        public string CustomerName { get; set; }

        [JsonProperty("customerIdType")]
        [Required(ErrorMessage = "入住人证件类型不能为空")]
        public string CustomerIdType { get; set; }  //字符串表示js传来的枚举

        [JsonProperty("customerIdNumber")]
        [Required(ErrorMessage = "入住人证件号不能为空")]
        public string CustomerIdNumber { get; set; }
    }
}
