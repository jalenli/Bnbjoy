﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Dashboard
{
    public class HeadInfoResponse
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string BnbId { get; set; }

        public string BnbName { get; set; }

        public bool OnlineSale { get; set; }

        public bool OnSelected { get; set; }
    }
}
