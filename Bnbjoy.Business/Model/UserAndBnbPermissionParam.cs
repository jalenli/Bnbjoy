﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model
{
    public class UserAndBnbPermissionParam
    {
        [JsonProperty("userId")]
        [Required(ErrorMessage = "UserId不能为空")]
        public string UserId { get; set; }

        [JsonProperty("role")]
        [Required(ErrorMessage = "Role不能为空")]
        public string Role { get; set; }

        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "BnbId不能为空")]
        public string BnbId { get; set; }
    }
}
