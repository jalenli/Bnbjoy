﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model
{
    public class LoginUserParam
    {
        [JsonProperty("userName")]
        [Required(ErrorMessage = "用户名不能为空")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        [Required(ErrorMessage = "密码不能为空")]
        [RegularExpression(@"(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}", ErrorMessage = "密码应为8-30位的字母、数字和特殊字符组成")]
        public string Password { get; set; }

        [JsonProperty("store30Days")]
        public bool Store30Days { get; set; }
    }
}
