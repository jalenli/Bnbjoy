﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Account
{
    public class OwnerAndEmployeeModel
    {
        public string OwnerId { get; set; }

        public string OwnerName { get; set; }

        public string OwnerEmail { get; set; }

        public string OwnerMobile { get; set; }

        public string OwnerRealName { get; set; }

        public string OwnerIdCard { get; set; }

        public string OwnerCert { get; set; }

        public IEnumerable<EmployeeModel> Employees { get; set; }
    }
}
