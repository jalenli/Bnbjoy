﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Account
{
    public class AddRemoveBnbPermissionParam
    {
        [JsonProperty("userId")]
        [Required(ErrorMessage = "UserId不能为空")]
        public string UserId{get; set;}

        [JsonProperty("items")]
        [Required(ErrorMessage = "Items不能为空")]
        public List<BnbPermissionItem> Items { get; set; }
    }

    public class BnbPermissionItem 
    {
        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "BnbId不能为空")]
        public string BnbId{get; set;}

        [JsonProperty("value")]
        [Required(ErrorMessage = "Value不能为空")]
        public bool Value { get; set; }
    }
}
