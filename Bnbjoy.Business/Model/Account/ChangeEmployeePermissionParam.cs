﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Account
{
    public class ChangeEmployeePermissionParam
    {
        [JsonProperty("userId")]
        [Required(ErrorMessage = "UserId不能为空")]
        public string UserId { get; set; }

        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "BnbId不能为空")]
        public string BnbId { get; set; }

        [JsonProperty("permissionStr")]
        [Required(ErrorMessage = "PermissionStr不能为空")]
        public string PermissionStr { get; set; }
    }
}
