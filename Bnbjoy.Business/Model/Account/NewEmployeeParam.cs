﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Account
{
    public class NewEmployeeParam
    {
        [JsonProperty("roleName")]
        [Required(ErrorMessage = "RoleName不能为空")]
        public string RoleName { get; set; }

        [JsonProperty("userName")]
        [Required(ErrorMessage = "用户名不能为空")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        [Required(ErrorMessage = "密码不能为空")]
        [RegularExpression(@"(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}", ErrorMessage = "密码应为8-30位的字母、数字和特殊字符组成")]
        public string Password { get; set; }

        [JsonProperty("userRealName")]
        [Required(ErrorMessage = "姓名不能为空")]
        [RegularExpression(@"^[\u4e00-\u9fa5 ]{2,20}$", ErrorMessage="姓名格式错误")]
        public string RealName { get; set; }

        [JsonProperty("email")]
        [Required(ErrorMessage = "邮箱不能为空")]
        [RegularExpression(@"^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$", ErrorMessage = "邮箱格式不正确")]
        public string Email { get; set; }

        [JsonProperty("mobileNumber")]
        [Required(ErrorMessage = "手机号不能为空")]
        [RegularExpression(@"^1[3|4|5|7|8]\d{9}$", ErrorMessage = "手机号格式错误")]
        public string Mobile { get; set; }

        [JsonProperty("ownerId")]
        [Required(ErrorMessage = "OwnerId不能为空")]
        public string OwnerId { get; set; }
    }
}
