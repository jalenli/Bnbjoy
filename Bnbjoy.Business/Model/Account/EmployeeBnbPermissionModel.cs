﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Account
{
    public class EmployeeBnbPermissionModel
    {
        public string UserId { get; set; }

        public string BnbId { get; set; }

        public string BnbName { get; set; }

        public bool Auth { get; set; }
    }

    public class EmployeeBnbPermissionRequestParam 
    {

        [JsonProperty("userId")]
        [Required(ErrorMessage = "UserId不能为空")]
        public string UserId { get; set; }
    }
}
