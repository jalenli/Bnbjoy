﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.Account
{
    public class EmployeeModel
    {
        public string EmployeeId { get; set; }

        public string OwnerId { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeEmail { get; set; }

        public string EmployeeMobile { get; set; }

        public string EmployeeRealName { get; set; }

        public bool Enabled { get; set; }

        public IEnumerable<PermissionModel> EmployeePermission { get; set; }
    }

    public class PermissionModel 
    {
        public string BnbId { get; set; }
        
        public string BnbName { get; set; }

        public string EmployeeRights { get; set; }
    }
}
