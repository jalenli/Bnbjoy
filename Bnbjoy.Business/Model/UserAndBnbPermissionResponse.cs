﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model
{
    public class UserAndBnbPermissionResponse
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

        public string ProfileId { get; set; }

        public string Permission { get; set; }

        public string BnbId { get; set; }

    }
}
