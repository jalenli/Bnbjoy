﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class ListCustomDailyPriceParam
    {
        [JsonProperty("fromTime")]
        [Required(ErrorMessage = "起始时间不能为空")]
        public string FromTime { get; set; }

        [JsonProperty("toTime")]
        [Required(ErrorMessage = "截止时间不能为空")]
        public string ToTime { get; set; }

        [JsonProperty("roomTypeId")]
        [Required(ErrorMessage = "房型Id不能为空")]
        public string RoomTypeId { get; set; }
    }
}
