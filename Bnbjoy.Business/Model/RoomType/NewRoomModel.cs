﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class NewRoomModel
    {
        [JsonProperty("roomNumber")]
        [Required(ErrorMessage = "房号不能为空")]
        [StringLength(10, ErrorMessage = "房号不超过10个字符")]
        public string RoomNumber { get; set; }

        [JsonProperty("bnbId")]
        [Required(ErrorMessage = "BnbId不能为空")]
        public string BnbId { get; set; }

        [JsonProperty("enabled")]
        [Required(ErrorMessage = "房间状态不能为空")]
        public bool Enabled { get; set; }
    }
}
