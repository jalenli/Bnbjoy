﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class QueryAllRoomTypesResponse
    {
        [JsonProperty("roomTypes")]
        public List<RoomTypeResponse> roomTypes { get; set; }
    }
}
