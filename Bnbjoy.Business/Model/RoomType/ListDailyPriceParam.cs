﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class ListDailyPriceParam
    {
        [JsonProperty("year")]
        [Required(ErrorMessage = "年份不能为空")]
        public int Year { get; set; }

        [JsonProperty("month")]
        [Required(ErrorMessage = "月份不能为空")]
        public int Month { get; set; }

        [JsonProperty("roomTypeId")]
        [Required(ErrorMessage = "房型Id不能为空")]
        public string RoomTypeId { get; set; }
    }
}
