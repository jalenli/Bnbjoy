﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using  Bnbjoy.Domain.Entities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class RoomTypeResponse
    {
        [JsonProperty("roomType")]
        public Bnbjoy.Domain.Entities.RoomType roomType { get; set; }

        [JsonProperty("rooms")]
        public IEnumerable<Bnbjoy.Domain.Entities.Room> rooms { get; set; }

        [JsonProperty("commonPrice")]
        public Bnbjoy.Domain.Entities.CommonPrice commonPrice { get; set; }
    }
}
