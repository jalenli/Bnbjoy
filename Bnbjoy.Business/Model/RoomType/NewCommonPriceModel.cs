﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Model.RoomType
{
    public class NewCommonPriceModel
    {
        [JsonProperty("initPrice")]
        [Required(ErrorMessage = "初始默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal InitPrice { get; set; }

        [JsonProperty("mondayPrice")]
        [Required(ErrorMessage = "周一默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal MondayPrice { get; set; }

        [JsonProperty("tuesdayPrice")]
        [Required(ErrorMessage = "周二默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal TuesdayPrice { get; set; }

        [JsonProperty("wednesdayPrice")]
        [Required(ErrorMessage = "周三默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal WednesdayPrice { get; set; }

        [JsonProperty("thursdayPrice")]
        [Required(ErrorMessage = "周四默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal ThursdayPrice { get; set; }

        [JsonProperty("fridayPrice")]
        [Required(ErrorMessage = "周五默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal FridayPrice { get; set; }

        [JsonProperty("saturdayPrice")]
        [Required(ErrorMessage = "周六默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal SaturdayPrice { get; set; }

        [JsonProperty("sundayPrice")]
        [Required(ErrorMessage = "周日默认价不能为空")]
        [Range(typeof(Decimal), "1", "9999", ErrorMessage = "价格范围应为1->9999")] 
        public decimal SundayPrice { get; set; }
    }
}
