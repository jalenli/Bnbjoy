﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Constants
{
    public enum SpenItemType
    {
        RoomFee = 1,
        Deposit = 2,
        Activity = 3,
        Others = 4
    }
}
