﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Constants
{
    public enum ChannelType
    {
        Online = 1,   //线上
        Offline = 2   //线下
    }
}
