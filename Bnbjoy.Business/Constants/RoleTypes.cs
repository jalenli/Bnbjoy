﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Constants
{
    public enum RoleTypes
    {
        SA = 1,
        BA = 2,
        BE = 3,
        EU = 4
    }

    public class Constants 
    {
        public const string DashboardManagement = "dashmgmt";
        public const string OrdersInfoManagement = "ordermgmt";
        public const string BnbInfoManagement = "infomgmt";
        public const string RoomInfoManagement = "roommgmt";
        public const string SocialManagement = "socialmgmt";
        public const string CustomerInfoManagement = "customermgmt";
        public const string StatisticsInfoManagement = "statsmgmt";
        public const string SettingsManagement = "settingsmgmt";
    }

}
