﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Constants
{
    public enum OrderType
    {
        BO = 1, //住店
        AO = 2, //活动
        PO = 3  //套餐
    }
}
