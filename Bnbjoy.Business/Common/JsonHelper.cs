﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Common
{
    public class JsonHelper
    {
        public static string ConvertObjectToJson(dynamic obj) 
        {
            var settings = new JsonSerializerSettings();
            //属性首字母小写
            settings.ContractResolver = new LowercaseContractResolver();
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented, settings);
            return json;
        }
    }
}
