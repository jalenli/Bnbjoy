﻿using Bnbjoy.Business.Abstract;
using Bnbjoy.Business.Model.Dashboard;
using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Concrete;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bnbjoy.Business.Concrete
{
    public class DashboardService<T> : IDashboardService where T : class
    {
        IUserRepository _userRepository;

        public DashboardService(T t)
        {
            if (typeof(T) == typeof(IUserRepository))
            {
                _userRepository = new UserRepository();
            }
        }

        public async Task<HeadInfoResponse> DashHeadInfo(string userId) 
        {
            dynamic dashUserInfo = await _userRepository.DashUserInfo(userId);
            string json = JsonConvert.SerializeObject(dashUserInfo);
            var hip = JsonConvert.DeserializeObject<HeadInfoResponse>(json);

            return hip;
        }

        public void Dispose()
        {

        }
    }
}
