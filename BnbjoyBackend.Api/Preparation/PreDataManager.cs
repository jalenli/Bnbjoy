﻿using Bnbjoy.Business.Abstract;
using Bnbjoy.Business.Concrete;
using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Concrete;
using Bnbjoy.Domain.Entities;
using BnbjoyBackend.Api.Constants;
using BnbjoyBackend.Api.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BnbjoyBackend.Api.Preparation
{
    public class PreDataManager
    {
        private static PreDataManager manager;
        private static object lockObj = new object();

        private PreDataManager()
        { }

        public static PreDataManager Instance
        {
            get
            {
                if (manager == null)
                {
                    lock (lockObj)
                    {
                        if (manager == null)
                        {
                            manager = new PreDataManager();
                        }
                    }
                }

                return manager;
            }
        }

        public void SeedData()
        {
            //预设置客户端信息
            this.SeedClients();
            //预设置角色信息
            this.SeedRoles();
            //预设置权限信息
            this.SeedPermissions();
        }

        private void SeedClients()
        {
            List<Client> clientsList = new List<Client> 
            {
                new Client
                { 
                    Id = "Web", 
                    Secret= SecurityManager.Instance.GetHash("Web@bnbjoy"), 
                    Name="WebSite User For Bnbjoy", 
                    ApplicationType =  (int)ApplicationTypes.Web, 
                    Active = true, 
                    RefreshTokenLifeTime = 48 * 60 * 60, //web端保存两天
                    AllowedOrigin = "*"
                },
                new Client
                { 
                    Id = "H5", 
                    Secret=SecurityManager.Instance.GetHash("H5@bnbjoy"), 
                    Name="Mobile Browser User For Bnbjoy", 
                    ApplicationType = (int)ApplicationTypes.H5, 
                    Active = true, 
                    RefreshTokenLifeTime = 48 * 60 * 60,  //移动浏览器用户保存两天
                    AllowedOrigin = "*"
                },
                new Client
                {
                    Id = "iOSApp",
                    Secret = SecurityManager.Instance.GetHash("iOSApp@bnbjoy"),
                    Name="iOS Native App User",
                    ApplicationType = (int)ApplicationTypes.iOS,
                    Active = true,
                    RefreshTokenLifeTime = 30 * 24 * 60 * 60, //iOS app用户保存一个月
                    AllowedOrigin = "*"
                },
                new Client
                {
                    Id = "AndoridApp",
                    Secret = SecurityManager.Instance.GetHash("AndroidApp@bnbjoy"),
                    Name="Andorid Native App User",
                    ApplicationType =(int)ApplicationTypes.Android,
                    Active = true,
                    RefreshTokenLifeTime = 30 * 24 * 60 * 60, //安卓 app用户保存一个月
                    AllowedOrigin = "*"
                },
                
            };

            using (IClientService clientService = new ClientService(new ClientRepository()))
            {
                clientService.AddClients(clientsList);
            }
        }

        private void SeedRoles()
        {
            List<Role> roleList = new List<Role> 
            {
                new Role
                {
                    RoleName = "SA" //super admin
                },
                new Role
                {
                    RoleName = "BA" //bnb admin
                },
                new Role
                {
                    RoleName = "BE" //bnb employee
                },
                new Role
                {
                    RoleName = "EU" //end user
                }
            };

            using (IAccountService accountService = new AccountService<IRoleRepository>(new RoleRepository()))
            {
                foreach (var role in roleList)
                {
                    accountService.AddRole(role);
                }
            }
        }

        private void SeedPermissions()
        {
            List<Permission> permissionList = new List<Permission>
            {
                new Permission
                {
                    PermissionTitle = "Dashboard"
                },
                new Permission
                {
                    PermissionTitle = "Order"
                },
                new Permission
                {
                    PermissionTitle = "Infomation"
                },
                new Permission
                {
                    PermissionTitle = "Room"
                },
                new Permission
                {
                    PermissionTitle = "Social"
                },
                new Permission
                {
                    PermissionTitle = "Customer"
                },
                new Permission
                {
                    PermissionTitle = "Statistics"
                },
                new Permission
                {
                    PermissionTitle = "Settings"
                },
            };

            using (IAccountService accountService = new AccountService<IPermissionRepository>(new PermissionRepository()))
            {
                foreach (var permission in permissionList)
                {
                    accountService.AddPermission(permission);
                }
            }
        }
    }
}