﻿using Bnbjoy.Business.Abstract;
using Bnbjoy.Business.Concrete;
using Bnbjoy.Business.Model.Dashboard;
using Bnbjoy.Domain.Abstract;
using Bnbjoy.Domain.Concrete;
using BnbjoyBackend.Api.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BnbjoyBackend.Api.Controllers
{
    public class DashboardController : ApiController
    {
        [Route("api/dashboard/headinfo")]
        [HttpPost]
        [Authorize(Roles = "BA,BE")]
        public async Task<HttpPostResponse> HeadInfo([FromBody]HeadInfoParam param) 
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                              .SelectMany(v => v.Errors)
                              .Select(e => e.ErrorMessage));
                return new HttpPostResponse(HttpStatusCode.BadRequest, message);
            }

            try
            {
                using (IDashboardService dashboardService = new DashboardService<IUserRepository>(new UserRepository()))
                {
                    var hip = await dashboardService.DashHeadInfo(param.UserId);
                    if (hip != null)
                    {
                        string hipJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(hip);
                        return new HttpPostResponse(HttpStatusCode.OK, hipJsonStr);
                    }
                    else
                    {
                        return new HttpPostResponse(HttpStatusCode.Forbidden, "获取数据失败");
                    }
                }
            }
            catch (Exception ex) 
            {
                return new HttpPostResponse(HttpStatusCode.InternalServerError, ex.Message + ex.InnerException);
            }
        }
    }
}
