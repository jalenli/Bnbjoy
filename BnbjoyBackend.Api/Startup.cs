﻿using Bnbjoy.Business.Abstract;
using Bnbjoy.Business.Concrete;
using Bnbjoy.Domain.Concrete;
using BnbjoyBackend.Api.Preparation;
using BnbjoyBackend.Api.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[assembly: OwinStartup(typeof(BnbjoyBackend.Api.Startup))]
namespace BnbjoyBackend.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            ConfigureOAuth(app);

            //这一行代码必须放在ConfiureOAuth(app)之后
            app.UseWebApi(config);
            //应用预先需要的数据
            PreDataManager.Instance.SeedData();
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            IRefreshTokenService refreshTokenService = new RefreshTokenService(new RefreshTokenRepository());

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                Provider = new SimpleAuthorizationServerProvider(),
                RefreshTokenProvider = new SimpleRefreshTokenProvider(refreshTokenService)
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}