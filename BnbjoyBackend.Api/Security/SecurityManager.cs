﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace BnbjoyBackend.Api.Security
{
    public class SecurityManager
    {
        private static SecurityManager manager;
        private static object lockObj = new object();

        private SecurityManager() { }

        public static SecurityManager Instance 
        {
            get
            {
                if (manager == null)
                {
                    lock (lockObj) 
                    {
                        if (manager == null) 
                        {
                            manager = new SecurityManager();
                        }
                    }
                }

                return manager;
            }
        }

        public string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();
            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);
            return Convert.ToBase64String(byteHash);
        }

        public string Md5AndBase64Encrypt(string str) 
        {
            System.Security.Cryptography.MD5 hs = System.Security.Cryptography.MD5.Create();
            byte[] db = hs.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));
            string result = Convert.ToBase64String(db);
            return result;
        }
    }
}