﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BnbjoyBackend.Api.Constants
{
    public enum ApplicationTypes
    {
        Web = 0,
        H5 = 1,
        iOS = 2,
        Android = 3
    }
}